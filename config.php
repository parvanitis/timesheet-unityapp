<?php

class MYCONFIG extends CONFIG{

  function __construct($runtime='',$login=TRUE) {
    parent::__construct($runtime,$login);

    $this->mainDB = 'timeapp_dbmain';
    $this->systemDB = 'timeapp_dbsys';
    $this->statement_ticket_short_timeout = 2;
    $this->statement_ticket_long_timeout = 30;
    $this->hosts['master']=array(
      'type'=>'mysql',
      'host'=>'127.0.0.1',
      'user'=>'root',
      'pass'=>'',
      'dsn'=>'mysql:host={$values[\'host\']};dbname='.$this->mainDB,
      'options'=>array(
        PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND =>'SET NAMES utf8'
      )
    );
    $this->dbTrace=1;
    $this->debug=true;
    $this->pagingMode=1;

    $this->portal='http://dev-mygalaxy.singularlogic.eu/';//'http://dev.singularlogic.eu/forest';
    $this->sso['info']=$this->portal.'/unity/sso/info';
    $this->sso['login']=$this->portal.'/unity/sso/login';
    $this->sso['logout']=$this->portal.'/user/logout';
    $this->sso['desktop'] = 'EVA';
    $this->session="unityEva";
    
    $this->serverPath = dirname(__FILE__) . '/server';
    $this->incPaths[]=$this->serverPath;

    $this->tenant=array('ID'=>1);
    $this->user=array('ID'=>1);

    $this->livereload=false;

  }
}


