define(function () {
    return {
        xtype: 'treemenu',
        items: [
            {
                group: true,
                items: [
                    {
                        text: 'Home', href: {cls: 'home'}
                    },
                    {
                        text: 'Projects',
                        collapsed: true,
                        items: [
                            {text: 'Show List', href: {cls: 'application/project/project-list'}},
                            {text: 'Add New', href: {cls: 'application/project/project'}}
                        ]
                    }
                ]
            }
        ]
    };
});