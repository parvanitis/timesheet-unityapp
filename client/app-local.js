/*
 * this file contains everything that is related to the particular project runtime
 */

define(['unity/app-formhandler', 'app-layout', 'app-dialogs', 'lib', 'app', 'unity/app-behaviors',
  'unity/app-listhandler', 'unity/slickgrid'], function() {

  // @@@ App settings
  app.classes.date.prototype.options.format = 'DD/MM/YYYY';

});