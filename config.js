define(['app', 'lib'], function () {
    app.config = {
        panel: true,
        panelAutoOpen: true
    };
    app.homeUrl = {
        cls: 'home'
    };
//    app.debug = false;
//    app.disableCache = false;
//    lib.dateType = 'string';
//    lib.number.ds = '.';
//    lib.number.dp = ',';
//    lib.autoMask = true;
//    lib.i18n.debug = false;
//    lib.logAjax = false;
//    lib.flags = false;
//    lib.DEBUG_MESSAGES = false;
    return app.config;
});